const {Router} = require('express');
const db = require('../services/db');
const Product = require('../models/Product');
const router = Router();
router.get('/:id/', (req, res) => {
  Product.matchById(parseInt(req.params.id)).then(product =>{
    let ok = typeof product === 'object';
    res.status(ok ? 200 : 206).send({success: ok, msg: ok ? 'success' : `ProductId: ${req.params.id} not found`, data: product || null});
  }).catch(err => {
    console.log('err',err);
    res.status(500).send({success: false, msg: err, data: null});
  });
});
router.get('/', (_ , res) => {
  Product.find({}).then(product => {
    res.status(200).send({success: true, msg: 'success', data: product});
  }).catch(err => {
    res.status(205).send({success: false, msg: err, data: null})
  });
});
router.put('/:id', (req , res) => {
    console.log(body);
    Product.query().matchById(parseInt(req.params.id)).patch(body).then(p => {
      res.status(200).send({success: true, msg: 'success', data: p});
    }).catch(err => {
      res.status(206).send({success: false, msg: err, data: null});
    });
});
module.exports = router;
