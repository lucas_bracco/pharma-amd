const path = require('path')
const dbPath = path.resolve(__dirname, '../data/database.db')
const {Model} = require('objection');
const db = require('knex')({
  client: 'sqlite3',
  connection: {
    filename: dbPath,
  },
  useNullAsDefault: true
});
(async _ => {
  try {
    // db.schema
    await db.schema
    // products
    .createTable('products', table => {
      table.increment('id').primary();
      table.text('name');
      table.float('price');
      table.float('price');
      table.text('location');
      table.integer('date', 11);
      table.boolean('tracked');
      table.boolean('enabled');
      table.text('gtin');
      table.text('img');
      table.text('concentration');
    })
    // providers
    .createTable('providers', table => {
      table.increment('id').primary();
      table.text('name');
      table.text('address');
      table.text('phone');
      table.text('email');
    })
    // batch
    .createTable('batch', table => {
      table.increment('id').primary();
      table.integer('nro');
      table.expiration('expiration', 11);
      table.expiration('date', 11);
    })
    // manufacturer
    .createTable('manufacturer', table => {
      table.increment('id').primary();
      table.text('name');
    })
    // category
    .createTable('category', table => {
      table.increment('id').primary();
      table.text('name');
    })
    // presentation
    .createTable('presentation', table => {
      table.increment('id').primary();
      table.text('name');
    })
    /////////// TABLAS RELACION ///////////
    .createTable('batch_products_presentation', table => {
      table.integer('idProduct').references('products.id');
      table.integer('idBatch').references('batch.id');
      table.integer('idPresentation').references('presentation.id');
    })
    .createTable('products_manufacturer', table => {
      table.integer('idProduct').references('products.id');
      table.integer('idManufacturer').references('manufacturer.id');
    })
    .createTable('products_activePrinciple', table => {
      table.integer('idProduct').references('products.id');
      table.integer('activePrinciple').references('activePrinciple.id');
    })
    .createTable('products_providers', table => {
      table.integer('idProduct').references('products.id');
      table.integer('idProviders').references('providers.id');
    })
    .createTable('providers_category', table => {
      table.integer('idProviders').references('providers.id');
      table.integer('idCategory').references('category.id');
    })
    //UTILS
    .createTable('sqlite_master', table => {
      table.text('name');
      table.text('type');
    });
    console.log('Database connected successfully.');

} catch(e){
  console.log(e);
}
});
Model.knex(db);
module.exports = {db, Model};
