const sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('../data/database.db', err => {
  if (err) {
    console.log('Error trying connect to database.');
    throw err;
  } else {
    console.log('Connection successfully.');
  }
});
let tables = [
  {
    name: 'products',
    fields: [
      ['id', 'INTEGER PRIMARY KEY AUTOINCREMENT'],
      ['name', 'TEXT'], //NOMBRE
      ['stock', 'INTEGER'], //DISPONIBILIDAD
      ['price', 'REAL'], // PRECIO
      ['location', 'TEXT'], // UBICACION EN EL ALMACEN
      ['date', 'INTEGER'], // FECHA DE CREACION
      ['tracked', 'INTEGER'], // ES TRAZABLE
      ['enabled', 'INTEGER'], // ESTA HABILITADO
      ['gtin', 'TEXT'], // CODIGO DE IDENTIFICACION - Global Trade Item Number
      ['img', 'BLOB'], // IMAGEN
      ['concentration', 'TEXT'] // CONCENTRACION DE LA DROGA
    ]
  },
  {
    name: 'providers',
    fields: [
      ['id', 'INTEGER PRIMARY KEY AUTOINCREMENT'],
      ['name', 'TEXT'],    //NOMBRE
      ['address', 'TEXT'], // DIRECCION
      ['phone', 'TEXT'],
      ['email', 'TEXT'],
    ]
  },
  {
    name: 'batch',
    fields: [
      ['id', 'INTEGER PRIMARY KEY AUTOINCREMENT'],
      ['nro', 'INTEGER'],    //NOMBRE
      ['expiration', 'INTEGER'],
      ['date', 'INTEGER']
    ]
  },
  {
    name: 'manufacturer',
    fields: [
      ['id', 'INTEGER PRIMARY KEY AUTOINCREMENT'],
      ['name', 'TEXT']
    ]
  },
  {
    name: 'activePrinciple',
    fields: [
      ['id', 'INTEGER PRIMARY KEY AUTOINCREMENT'],
      ['name', 'TEXT']
    ]
  },
  {
    name: 'category',
    fields: [
      ['id', 'INTEGER PRIMARY KEY AUTOINCREMENT'],
      ['name', 'TEXT']
    ]
  },
  {
    name: 'presentation',
    fields: [
      ['id', 'INTEGER PRIMARY KEY AUTOINCREMENT'],
      ['name', 'TEXT']
    ]
  }
];
let refTables = [
  {
    name: 'batch_products_presentation',
    fields: [
      ['idProduct', 'INTEGER references products(id)'],
      ['idBatch', 'INTEGER references batch(id)'],
      ['idPresentation', 'INTEGER references presentation(id)']
    ]
  },
  {
    name: 'products_manufacturer',
    fields: [
      ['idProduct', 'INTEGER references products(id)'],
      ['idManufacturer', 'INTEGER references manufacturer(id)']
    ]
  },
  {
    name: 'products_activePrinciple',
    fields: [
      ['idProduct', 'INTEGER references products(id)'],
      ['idActivePrinciple', 'INTEGER references activePrinciple(id)']
    ]
  },
  {
    name: 'products_providers',
    fields: [
      ['idProduct', 'INTEGER references products(id)'],
      ['idProviders', 'INTEGER references providers(id)']
    ]
  },
  {
    name: 'providers_category',
    fields: [
      ['Idproviders', 'INTEGER references providers(id)'],
      ['idCategory', 'INTEGER references category(id)']
    ]
  }
];
[...tables, ...refTables].forEach(table => {
    let tmp = `CREATE TABLE IF NOT EXISTS ${table.name} (${table.fields.map(x => x[0] + ' ' + x[1]).join(', ')})`;
    db.run(tmp)
});
module.exports = db;
