const db = require('../services/db');
const {Model} = require('objection');
Model.knex(db);

class Category extends Model {
  static tableName = 'manufacturer';
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name'],
      properties:{
        id: {type: ['integer', 'null']},
        name: {type: 'string'}
      }
    };
  }
};
module.exports = new Category();
