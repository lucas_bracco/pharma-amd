const {Model} = require('../services/db');

class Product extends Model {
  static tableName = 'products';
  static get idColumn(){
    return 'id';
  }
  static find(where={}){
    return this.query()
    .withGraphFetched('providers')
    .withGraphFetched('activePrinciples')
    .withGraphFetched('manufacturer')
    .where(this.fixEntry(where));
  }
  static matchById(id){
    return this.query()
    .findById(id)
    .withGraphFetched('providers')
    .withGraphFetched('activePrinciples')
    .withGraphFetched('manufacturer');
  }
  static update(where, query){
    return this.find(this.fixEntry(where)).patch(this.fixEntry(query));
  }
  static get relationMappings() {
    const ActivePrinciple = require('./ActivePrinciple');
    const Provider = require('./Provider');
    const Manufacturer = require('./Manufacturer');
    return {
      activePrinciples: {
        relation: Model.ManyToManyRelation,
        modelClass: ActivePrinciple,
        join: {
          from: 'products.id',
          through: {
            from: 'products_activePrinciple.idProduct',
            to: 'products_activePrinciple.idActivePrinciple'
          },
          to: 'activePrinciple.id'
        }
      },
      manufacturer: {
        relation: Model.ManyToManyRelation,
        modelClass: Manufacturer,
        join: {
          from: 'products.id',
          through: {
            from: 'products_manufacturer.idProduct',
            to: 'products_manufacturer.idManufacturer'
          },
          to: 'manufacturer.id'
        }
      },
      providers: {
        relation: Model.ManyToManyRelation,
        modelClass: Provider,
        join: {
          from: 'products.id',
          through: {
            from: 'products_providers.idProduct',
            to: 'products_providers.idProviders'
          },
          to: 'providers.id'
        }
      }
    };
  }
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['id'],
      properties:{
        id: {type: ['integer', 'null']},
        name: {type: 'text'},
        stock: {type: 'integer'},
        price: {type: 'real'},
        location: {type: 'text'},
        date: {type: 'integer'},
        tracked: {type: 'integer'},
        enabled: {type: 'integer'},
        gtin: {type: 'integer'},
        img: {type: 'text'},
        concentration: {type: 'text'},
      }
    };
  }
  static fixEntry(where){
    let whereOut = {};
    Object.keys(Product.jsonSchema.properties).filter(x => Object.keys(where).includes(x)).forEach(x => {
        whereOut[x] = where[x];
    });
    return whereOut;
  }
}
module.exports = Product;
