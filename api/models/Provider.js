const {Model} = require('../services/db');

class Provider extends Model {
  static tableName = 'providers';
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name'],
      properties:{
        id: {type: ['integer', 'null']},
        name: {type: 'string'},
        phone: {type: 'string'},
        dir: {type: 'string'},
        email: {type: 'string'}
      }
    };
  }
};
module.exports = Provider;
